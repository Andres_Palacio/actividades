//Este programa realiza una petición al servicio alojado en la URL https://api.helo.v1.intelmotics.com/services/prueba_tecnica/hola.php
//de tipo POST, Headers: Content-Type: application/x-www-form-urlencoded
//Se espera recibir 3 códigos como respuesta a 3 diferentes casos:
//code: 200, msg: “Bienvenido a Intelmotics”
//code: 500, msg: “No se han enviado los campos correctamente”
//code: 400, msg: “El id y/o el token son incorrectos”

#include <ESP8266WiFiMulti.h>               //Se incluye la libreria para realizar la conexión a internet
#include <ESP8266HTTPClient.h>              //Se incluye la libreria para realizar la petición al servicio

const char* ssid = "FAMILIA RUIZ";          //Se define el nombre de la red WiFi
const char* password = "JARBMW786";         //Se define la contraseña de la red WiFi

String id = "1212";                         //Se define uno de los parámetros a enviar (id)
String token = "1010101010";                //Se define uno de los parámetros a enviar (token)

void setup() {                              
   delay (50);
   Serial.begin(115200);

   WiFi.begin(ssid, password);               //Se inicia la conexión a la red WiFi
 
   Serial.print("Conectando al WiFi");       //Se indica que se esta conectando al WiFi
 
   while (WiFi.status() != WL_CONNECTED) {   //Se inicia un ciclo hasta que se realiza la conexión correctamente 
      delay(500);
      Serial.print(".");
   }

   Serial.println("Conectado");               //Se indica que se ha establecido la conexión WiFi
}

void loop() {

  if(WiFi.status() == WL_CONNECTED){          //Se verifica que la conexión WiFi este establecida
    
    HTTPClient http;                          //Se indica para poder enviar solicitudes HTTP
    String datos_a_enviar = "id=" + id + "&token=" + token; //Se organizan los datos que se van a enviar

    http.begin("http://api.helo.v1.intelmotics.com/services/prueba_tecnica/hola.php");  //Se indica la URL donde se encuentra alojado el servicio
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");                //Se indica el tipo de Header

    int code_respuesta = http.POST(datos_a_enviar);//Se crea una variable para almacenar el código de respuesta del servicio
                                                   //Se realiza la petición POST al servicio y se envian los datos organizados

    String msg_respuesta = http.getString(); //Se crea una variable que almacena la respuesta del servicio
    Serial.println(msg_respuesta);        //Se muestra por el monitor serial la respuesta del servicio

    http.end();                                 //Finalizamos para liberar recursos
  }else{
    Serial.println("Error en la conexión WiFi"); //Se muestra el mensaje en caso de no establecer la conexión WiFi
  }

  delay(60000);                                  //Se realiza una petición al servicio cada minuto 
}
