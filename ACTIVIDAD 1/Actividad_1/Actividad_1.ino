//Este programa realiza el sensado de corriente con un sensor no invasivo cuya relación 
//"Corriente sensada / Voltaje entregado" es de 100A/1V
//Esta programación se realiza sobre un microcontrolador que contiene al ESP12-E y el sensor
//de corriente esta conectado directamente al ADC del ESP8266 que opera a un Voltaje máximo
//de 1V y tiene una resolución de 10 bits (0-1024)

//El principio de funcionamiento del circuito se basa en la fórmula matemática para hallar el 
//valor RMS (valor eficaz) de una señal, que se calcula sumando los cuadrados de los valores 
//de la corriente instantanea de la señal y sacando la raiz cuadrada de la anterior suma divida
//entre el número de muestras de la corriente instantanea.

#define Muestras 4000                   //Definimos el número de muestras que vamos a tomar de la señal
                                        //Entre mayor el número de muestras, mayor precisión, pero más lento el proceso
                                        //En base a la cantidad de muestras, se puede calcular también los 5 segundos de muestreo
#define AmpMax 100                      //Definimos la corriente máxima que mide el sensor de corriente
#define VoltMax 1                       //Definimos el voltaje máximo que entrega el sensor de corriente

float corriente;                        //Creamos una variable float para almacenar e imprimir la corriente RMS sensada
int FC = 1;                             //Creamos una variable "Factor de Corrección" en caso de ser necesaria al momento
                                        //de realizar las pruebas, para ajustar el valor sensado. 
                                        //En caso de no ser utilizada la dejamos en "1" para no afectar el resultado.

void setup() {  
  Serial.begin(115200);
}

void loop() {
  corriente = Med_Irms() * FC;          //El valor de la corriente sensada va a ser calculado en Med_Irms() y de ser necesario
                                        //se multiplicará por un Factor de Corrección.
  
  Serial.print(corriente);              //Se imprime en el monitor serial el valor de la corriente sensada
  Serial.println(" A");                 //Se imprime la letra A de Amperios  
  delay(5000);                          //Se asigna un Delay de 5 segundos para realizar cada muestreo, sin embargo, esto puede variar
                                        //dependiendo del tiempo que se tarde el programa en calcular la corriente RMS basado 
                                        //en el número de muestras que se le asigno (4000 en este caso)
}

float Med_Irms(){                       
  long tiempoInicio = millis();          //Se define esta variable para medir el tiempo que tarda el programa en calcular la corriente RMS
                                         //basado en el número de muestras, con el fin de variar el delay, para obtener un muestreo exacto cada 5 segundos
  float Irms;                            //Se define la variable que se retornará con la corriente eficaz 
  float Iinst;                           //Se define la variable que almacena la Corriente Instantanea 
  float Vinst;                           //Se define la variable que almacena el Voltaje Instantaneo
  float sumIinst = 0;                    //Se define la variable que almacena la suma del cuadrado de las corrientes instantaneas

  for (int i=0; i < Muestras; i++){      //Se crea un ciclo for para realizar la toma de corrientes instantaneas (4000 veces en este caso)
    Vinst = analogRead(A0) * (1/1023.0); //Se lee por el puerto ADC el valor que entrega el sensor y se multiplica por el valor del Voltio por bit
    Iinst = Vinst * AmpMax / VoltMax;    //Se calcula la corriente instantanea con una regla de 3
    sumIinst += sq(Iinst);               //Se realiza la suma del cuadrado de las corrientes instantaneas hasta finalizar el muestreo
  }
  
  Irms = sqrt(sumIinst/Muestras);         //Finalmente se calcula la corriente RMS sacando la raiz cuadrada de la suma del cuadrado de las
                                          //corrientes instantaneas divido por el número de muestras (4000 en este caso)
                                          
  long tiempoFin = millis();              //Se define esta variable para medir el tiempo que tarda en calcular la corriente RMS
                                          //basado en el número de muestras para variar el delay y obtener un muestreo exactamente cada 5 segundos.
  //Serial.print(tiempoFin - tiempoInicio);//Si se desea, se puede mostrar por el monitor serial el tiempo que se tardó en realizar
                                          //el muestreo para ajustar el delay y obtener una lectura exactamente cada 5 segundos.
   
  return(Irms);                           //Se retorna el valor de la corriente RMS para ser mostrada por el monitor serial
}
